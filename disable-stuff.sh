#!/bin/bash

systemctl disable console-setup
systemctl disable keyboard-setup
systemctl disable avahi-daemon
systemctl disable pppd-dns
systemctl disable speech-dispatcher
systemctl disable NetworkManager-wait-online
systemctl disable ModemManager

