#!/bin/bash

# Use sha224. Should be reliable :P
# the first argument is the root of the starting directory
# if nothing passed then current directory (from where script is invoked) is used

echo -e "WARNING : If large files are present in directory tree or
	directory tree is huge then it'll take a long time 
	as this utililty does not do parallel processing. 
	(May add controlled parallel processing in future)
"
echo "Root of traversal : " $start_dir

read -p "Continue ? (y/n) : " resp

if [ $resp = "n" ]
then
	exit 0
fi

hash_store=/tmp/dup_checker.hashes
echo "Hashes" >  $hash_store

output_file=${2:-./duplicate_files}
echo "Duplicates" > $output_file

start_dir=${1:-.}

count=0
total=$(find $start_dir -type f | wc -l)

traverse() {
	cur_dir=$1

	find $cur_dir -type f -print | while read fl
	do
		hash_res=$(sha224sum "$fl")
		file_hash=$(echo $hash_res | gawk '{print $1}')

		res=$(grep $file_hash $hash_store)

		if [ $? -eq 0 ]
		then
			echo "$fl = $(echo $res | cut -d' ' -f 1 --complement)" >> $output_file
		else
			echo "$hash_res" >> $hash_store
		fi

		count=$(( count + 1 ))

		clear
		echo "Files Processed : $count out of $total"
	done
}

traverse "$start_dir"

rm $hash_store

