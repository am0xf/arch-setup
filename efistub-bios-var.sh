#! /bin/bash

uuid=$(/sbin/blkid -s UUID -o value /dev/nvme0n1p7)
os=arch
efibootmgr -v -d /dev/nvme0n1 -p 1 -c -L "$os" -l "\\EFI\\$os\\vmlinuz-linux" -u "root=UUID=$uuid rw initrd=\\EFI\\$os\\initramfs-linux.img"
