#!/bin/bash

# systemd units to watch kernel path for updates and update kernel stub in ESP
cp -v systemd-units/kernel-update* /etc/systemd/system/
chmod a-x /etc/systemd/system/kernel-update.*
systemctl daemon-reload
systemctl enable kernel-update.path

