#!/bin/bash

user=$1

cp -r utils/ /home/$user/

ln -s /home/$user/scripts/ion-login.sh /usr/bin/ion-login
chown $user:$user /usr/bin/ion-login
chmod 700 /usr/bin/ion-login

ln -s /home/$user/scripts/sync-xt.sh /usr/bin/sync-xt
chown $user:$user /usr/bin/sync-xt
chmod 700 /usr/bin/sync-xt

ln -s /home/$user/scripts/mnt-xt.sh /usr/bin/mnt-xt
chown $user:$user /usr/bin/mnt-xt
chmod 700 /usr/bin/mnt-xt

