#!/bin/bash

USER=aiden

cp scripts/ion-login.sh /usr/bin/ion-login
chown $USER:$USER /usr/bin/ion-login
chmod 700 /usr/bin/ion-login

cp scripts/sync-xt.sh /usr/bin/sync-xt
chown $USER:$USER /usr/bin/sync-xt
chmod 700 /usr/bin/sync-xt

