#!/bin/bash

user=$1

timedatectl set-ntp 1
timedatectl set-local-rtc 1
timedatectl set-timezone Asia/Kolkata
hwclock --systohc

./mkhome.sh $user

./disable-stuff.sh

pacman -Syyu

# kde
./install-kde.sh

# rem extras
./rem-pkgs.sh

# utils
./install-utils.sh

# blackarch repos
./blackarch.sh

# confs
./cp-confs.sh $user

# scripts
./cp-utils.sh $user

# systemd units
./cp-systemd-units.sh

