#!/bin/bash

pacman -S plasma-desktop sddm sddm-kcm gwenview kde-gtk-config bluedevil kgamma5 plasma-browser-integration plasma-nm plasma-pa powerdevil user-manager konsole dolphin kcalc
systemctl enable sddm
systemctl enable display-manager
systemctl enable NetworkManager
